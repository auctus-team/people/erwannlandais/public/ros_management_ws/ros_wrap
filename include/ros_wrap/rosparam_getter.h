#ifndef ROSPARAM_GETTER_H
#define ROSPARAM_GETTER_H

#include "ros/ros.h"

#include <string>
#include <sstream>

#include <vector>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/LU>

#include <XmlRpcValue.h>


namespace rosWrap
{
    void correctParam(std::string & s);    
    // necessary to put the whole code to be sure that the template will be found 
    // by other codes.
    template <class T>
    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param T& param_data the variable linked to the ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, T& param_data, int ifFailure=0, bool verbose = false)
    {
        bool gotParam = true;
        std::string correctpn  = param_name;
        correctParam(correctpn);
        if (!ros::param::get(correctpn, param_data))
        {
            if (ifFailure==0)
            {
                ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
                ros::shutdown();

            }
            else if (ifFailure==1)
            {
                ROS_WARN(" Problem loading parameters %s . If not voluntary, check the Yaml config file.",param_name.c_str());
            }
            gotParam = false;
        }
        else
        {
            if (verbose)
            {
                ROS_INFO_STREAM(param_name << " : " << param_data);
                //std::stringstream a;
                //std::cout << param_data << std::endl;
                //ROS_INFO_STREAM( a.str().c_str());
            }
        }
        return gotParam;
    };

    template <class T>
    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param T& param_data the variable linked to the ros param
        * \param ifFailure : 0 : shutdown code; 1 : only returns that parameter cannot be found, keep its values
        * as they were before code.
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, std::vector<T>& param_data, int ifFailure=0, bool verbose = false)
    {
        bool gotParam = true;        
        std::vector<T> std_param;
        std::string correctpn  = param_name;
        correctParam(correctpn);
    
        if (!ros::param::get(correctpn, std_param))
        {
            if (ifFailure==0)
            {
                ROS_FATAL_STREAM(" Problem loading parameters " << correctpn << ". Check the Yaml config file. Killing ros");
                ros::shutdown();

            }
            else if (ifFailure==1)
            {
                ROS_WARN(" Problem loading parameters %s . If not voluntary, check the Yaml config file.",correctpn.c_str());
            }
            gotParam = false;
        }
        else
        {

            param_data = std_param;

            if (verbose)
            {
                ROS_INFO_STREAM(correctpn << " : ");
                for (int i = 0; i< param_data.size(); i++)
                {
                    ROS_INFO_STREAM(param_data[i]);
                }
            }
        }

        return(gotParam);

    };

    template <class T>
    /**
     * Load a list of lists at any depth
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param T& param_data the variable linked to the ros param
        * \return true if the ros param exist and can be linked to the variable
        */
   bool getRosParam(std::vector<std::string> & all_param_names, XmlRpc::XmlRpcValue obj, std::vector<std::vector<T> >& param_data, int ifFailure=0, bool verbose = false, int index = 0)
    {
        bool gotParam = false;        
        std::vector< std::vector<T> > std_param;
        XmlRpc::XmlRpcValue child_obj;
        bool gotObj = false;
        if (index == -1)
        {
            child_obj = obj;
            index = all_param_names.size()-1;
            gotObj = true;
        }
        else
        {
            std::string param_name = all_param_names[index];
            correctParam(param_name);
            all_param_names[index] = param_name;
            if (obj[param_name].getType() == XmlRpc::XmlRpcValue::TypeStruct)
            {
                child_obj = obj[all_param_names[index]];
                gotObj = true;
            }            
        }
        if (gotObj)
        {
            // std::cout << "gotObj" << std::endl;
            if (index == all_param_names.size()-1 )
            {
                // if (verbose)
                // {
                //     std::cout << "child size : " << child_obj.size() << std::endl;
                // }
                for (int i=0; i < child_obj.size(); i++)
                {
                    std::vector<T> v;
                    for (int j = 0; j < child_obj[i].size(); j++)
                    {
                        // if (verbose) { std::cout <<child_obj[i][j] << std::endl; }
                        v.push_back(child_obj[i][j]);
                    }
                    param_data.push_back(v);
                }
                return(true);
            }
            else
            {
                gotParam = getRosParam(all_param_names,child_obj,param_data,ifFailure,verbose,index+1);
            }
        }
        else
        {
            if (ifFailure==0)
            {
                ROS_FATAL_STREAM(" Problem loading parameters " << all_param_names[index] << " at depth " << index << ". Check the Yaml config file. Killing ros");
                ros::shutdown();

            }
            else if (ifFailure==1)
            {
                ROS_WARN(" Problem loading parameters %s  at depth %d. If not voluntary, check the Yaml config file.",all_param_names[index].c_str(), index);
            }
            gotParam = false;
        }        

        return(gotParam);

    }

    template <class T>
    /**
     * Load a list of lists
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param T& param_data the variable linked to the ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(std::vector<std::string> & all_param_names, std::vector<std::vector<T> >& param_data, int ifFailure=0, bool verbose = false, int index = 0)
    {
        bool gotParam = false;        
        std::vector< std::vector<T> > std_param;
        std::string param_name = all_param_names[index];
        correctParam(param_name);
        all_param_names[index] = param_name;
        if (ros::param::has(param_name))
        {
            XmlRpc::XmlRpcValue obj;
            ros::param::get(param_name, obj);
            if (obj.getType() == XmlRpc::XmlRpcValue::TypeStruct )
            {     
              if (index == all_param_names.size() -1)
              {
                //std::cout << "special_case" << std::endl;
                // will use directly obj to get data
                gotParam = getRosParam(all_param_names,obj,param_data,ifFailure,verbose,-1);
              }
              else
              {
                // std::cout << "index : " << index << std::endl;
                // std::cout << all_param_names.size() - 1 << std::endl;
                gotParam = getRosParam(all_param_names,obj,param_data,ifFailure,verbose,index+1);
              }
            }
            else
            {
                // will use directly obj to get data
                gotParam = getRosParam(all_param_names,obj,param_data,ifFailure,verbose,-1);
            }

        }

        else
        {
            if (ifFailure==0)
            {
                ROS_FATAL_STREAM(" Problem loading parameters " << param_name << " at depth " << index << ". Check the Yaml config file. Killing ros");
                ros::shutdown();
            }
            else if (ifFailure==1)
            {
                ROS_WARN(" Problem loading parameters %s  at depth %d. If not voluntary, check the Yaml config file.", param_name.c_str(), index);
            }
            gotParam = false;
        }


        if (verbose && gotParam)
        {
            std::string param_name = "";
            for (int i = 0; i < all_param_names.size(); i++)
            {
                param_name += all_param_names[i] + "/";
            }
            ROS_INFO_STREAM(param_name << " : ");
            for (int i = 0; i< param_data.size(); i++)
            {
                std::stringstream ss;
                ss << "[ ";
                for (int j = 0; j < param_data[i].size(); j++)
                {
                    ss << param_data[i][j];
                    if (j != param_data.size() -1)
                    {
                        ss << " , ";
                    }
                    else
                    {
                        ss << " ]";
                    }
                }

                ROS_INFO_STREAM(ss.str().c_str());
            }
        }


        return(gotParam);

    };

    bool getRosParam(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose = false);


    bool getRosParamSizeCheck(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose = false);

    void splitString(std::string s, std::string delimiter, std::vector<std::string> & v);

    std::string getEnvVar( std::string & key );
}

#endif