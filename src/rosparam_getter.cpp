#include "ros_wrap/rosparam_getter.h"



namespace rosWrap
{
    // From https://stackoverflow.com/questions/631664/accessing-environment-variables-in-c
    /*
    Get the value of any env variable. Useful if you need a env parameter
    to set variables in .yaml file.    
    */
    std::string getEnvVar( std::string & key ) 
    {
        char * val = getenv( key.c_str() );
        return val == NULL ? std::string("") : std::string(val);
    }


    void splitString(std::string s, std::string delimiter, std::vector<std::string> & v)
    {
        v.clear();
        size_t pos = 0;
        std::string token;
        while ((pos = s.find(delimiter)) != std::string::npos) {
            token = s.substr(0, pos);
            v.push_back(token);
            s.erase(0, pos + delimiter.length());
        }
        v.push_back(s);
        //std::cout << s << std::endl;

    }

    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param Eigen::VectorXd& param_data a Eigen vector linked to the std::vector ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParamSizeCheck(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose)
    {
        std::vector<double> std_param;
        std::string correctpn  = param_name;
        correctParam(correctpn);
        if (!ros::param::get(param_name, std_param))
        {
            ROS_FATAL_STREAM(" Problem loading parameters " << correctpn << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        else
        {
        if (std_param.size() == param_data.size())
        {
            for (int i = 0; i < param_data.size(); ++i)
            param_data(i) = std_param[i];
            if (verbose)
            ROS_INFO_STREAM(correctpn << " : " << param_data.transpose());
            return true;
        }
        else
        {
            ROS_FATAL_STREAM("Wrong matrix size for param " << correctpn << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        }
        return(true);
    };


    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param Eigen::VectorXd& param_data a Eigen vector linked to the std::vector ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose)
    {
        std::vector<double> std_param;
        std::string correctpn  = param_name;
        correctParam(correctpn);
    
        if (!ros::param::get(correctpn, std_param))
        {
            ROS_FATAL_STREAM(" Problem loading parameters " << correctpn << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
            return(false);
        }
        else
        {

            Eigen::VectorXd vec(std_param.size(),1);
        
            for (int i = 0; i < std_param.size(); ++i)
            {
            vec(i) = std_param[i];
            }
            param_data = vec;

            if (verbose)
            ROS_INFO_STREAM(correctpn << " : " << param_data.transpose());
            return true;
        }

    };

    void correctParam(std::string & s)
    {
        //ROS_INFO_STREAM( "before : " << s);
        std::vector<std::string> v;
        splitString(s,"/",v);
        std::string s2 = "";
        for (int i = 0; i < v.size(); i++)
        {
            //std::cout << v[i] << std::endl;
            if (v[i].size() != 0)
            {
                s2+= "/" + v[i];
            }
        }
        s = s2;    
        //ROS_INFO_STREAM( "after : " << s);      
    }
};


