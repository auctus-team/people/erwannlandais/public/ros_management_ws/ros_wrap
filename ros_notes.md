# Debug

## Communication from roscpp client to rospy service

https://stackoverflow.com/questions/41878657/roscpp-client-fails-to-call-rospy-service

NEVER put a number after the service name!!

Ok : calibResp = nh.serviceClient<robot_to_frame::ReturnCalib>("/R2TF_resp");
KO : calibResp = nh.serviceClient<robot_to_frame::ReturnCalib>("/R2TF_resp",1);
