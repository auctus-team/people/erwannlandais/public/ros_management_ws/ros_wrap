Package that contains libraries in Cpp and Python with generic useful functions for ros (ex : time management for messages, service calls, ...).
The idea is to stop to repeat all the time those methods, and to simply call them whenever it's needed.

What is done now : 

* rosparam_getter (C++) : manages the recovering of ros parameters (from .yaml).
* com_manager (Py) : manages at a higher level the use of ROS services.
