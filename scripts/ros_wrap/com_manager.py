import rospy


class comManager():

    def __init__(self):
        a = 1

    def call_service(self,service_name,service, request, vmax=1000,verbose = False):
        v = 0
        resp1 = 0
        success = False
        ret = None
        if verbose:
            rospy.loginfo("Begin call to %s..."%service_name)
        while (not rospy.is_shutdown() ) and (resp1 == 0):
            try:
                ret = service(request)
                try:
                    success = ret.success
                except Exception:
                    success = True
                resp1 = 1

                if (success and verbose):
                    rospy.loginfo("Call to %s successful!"%service_name)
                elif not success and vmax >= 1000:
                    rospy.loginfo("Call to %s worked, but result failed."%service_name)
            except Exception as e :
                #rospy.loginfo("HERE")
                v+=1
                
                if (v%100) == 0:
                    if (verbose):
                        rospy.loginfo("Still try to call %s..."%service_name)
                if v> vmax:
                    rospy.loginfo("Error : too many failures into calling service %s. Please retry."%service_name)
                    resp1 = -1
                    success = False
            # except Exception:
            #     rospy.loginfo("HERE")
            #     resp1 = 0
                
        return(success,resp1,ret)
    

    def call_multiple_services(self,L_services,L_names,L_req):
        allSet = True
        i = 0
        while i  < len(L_services) and allSet:
            success,resp1,ret = self.call_service(L_names[i], L_services[i], L_req[i])
            if not success:
                allSet = False
            i+=1
        return(allSet)    